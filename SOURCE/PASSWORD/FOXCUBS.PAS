{

        Fox-cubs v0.1 by Mateusz Viste "Fox" / the.killer@wp.pl

                  http://the.killer.webpark.pl


 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2.1
 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

}

Unit FoxCubs;

INTERFACE

Uses DOS;

var N1,N2: ShortInt;

Function LoadMessage(N1,N2: ShortInt):String;

Implementation

Function LoadMessage(N1,N2: ShortInt):String;

var x: ShortInt;
    y: Integer;
    N3,N4: ShortInt;
    NLSpath, NLSfile, line, temp, WorkString: String;
    Lang: String[2];
    FileHandler: text;
    CurDir: DirStr;
    CurName: NameStr;
    Ext: ExtStr;

Label SkipComment;
Begin
 FSplit(ParamStr(0), CurDir, CurName, Ext);

  NLSfile := '';
  NLSpath := GetEnv('NLSPATH');
  Lang := GetEnv('LANG');
  if (NLSpath = '') or (FSearch(CurName+'.'+Lang,NLSpath) = '') or (FSearch(CurName,NLSpath+'\'+Lang) = '') then
       if FSearch(CurName+'.'+Lang,CurDir) <> '' then NLSfile := CurDir+'\'+CurName+'.'+Lang
     else
      Begin
       if FSearch(CurName+'.'+Lang,NLSpath) <> '' then NLSfile := FExpand(FSearch(CurName+'.'+Lang,NLSpath));
       if FSearch(CurName,NLSpath+'\'+Lang) <> '' then NLSfile := FExpand(FSearch(CurName,NLSpath+'\'+Lang));
  end;

 WorkString := '';

 if NLSfile <> '' then
    Begin
     Assign(FileHandler,NLSfile);
     reset(FileHandler);
     repeat
      readln(FileHandler,line);
      if line[1]='#' then goto SkipComment;
      x := 0;
      temp := '';
      repeat
        Inc(x);
        if line[x] <> '.' then temp:=temp+line[x];
      until (line[x]='.') or (x>=length(line));
      if x>=length(line) then goto SkipComment;
      VAL (temp,N3,y);
      temp := '';
      repeat
        Inc(x);
        if line[x] <> ':' then temp := temp+line[x];
      until (line[x]=':') or (x>=length(line));
      Val(temp,N4,y);
      temp := '';
      For y := x+1 to Length(line) do temp := temp+line[y];
      if (N1=N3) and (N2=N4) then WorkString := temp;
      SkipComment:
     until (eof(FileHandler)) or (WorkString <> '');
     close(FileHandler);
    End;
 LoadMessage := WorkString;
 End;

End.
